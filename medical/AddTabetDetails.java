package medical;
import java.sql.*;
import javax.swing.*;

public class AddTabetDetails {
	int codenumber;
	String tabletname;
	int quantity;

	public AddTabetDetails(int codenumber,String tabletname,int quantity) {
		
		this.codenumber = codenumber;
		this.tabletname = tabletname;
		this.quantity   = quantity;
		
	}

	public static void main(String[] args) throws SQLException {

		AddTabetDetails tablet=new AddTabetDetails(651446888,"dolo",100);
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			String url="jdbc:oracle:thin:@localhost:1521:xe";
			String user="system";
			String password="admin";
					
			Connection con=DriverManager.getConnection(url, user, password);
			PreparedStatement insertTablet=con.prepareStatement("insert into tabletdetails values(?,?,?) "); 
			
			insertTablet.setInt(1,tablet.codenumber);
			insertTablet.setString(2,tablet.tabletname);
			insertTablet.setInt(3,tablet.quantity);
			
			int i=insertTablet.executeUpdate();
			
			System.out.println("code   number : "+tablet.codenumber);
			System.out.println("tablet name : "+tablet.tabletname);
			System.out.println("tablet name : "+tablet.quantity+"   are added");
			
			con.close();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		 
	}
}
