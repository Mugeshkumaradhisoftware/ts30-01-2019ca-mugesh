package medical;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.*;

public class SearchTabletQuantity {
	
	String searchKeyWord;

	public SearchTabletQuantity(String searchKeyWord) {
		this.searchKeyWord=searchKeyWord;
	}

	public static void main(String[] args) throws SQLException {
		SearchTabletQuantity tablet=new SearchTabletQuantity("a");
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			String url="jdbc:oracle:thin:@localhost:1521:xe";
			String user="system";
			String password="admin";
					
			Connection con=DriverManager.getConnection(url, user, password);
			Statement stmt=con.createStatement();
			String key=tablet.searchKeyWord;
			
			ResultSet searchResult=stmt.executeQuery("select * from tabletdetails where tablename like '"+key +"%' ");
//			int temp=0;
			while(searchResult.next()) {
//				temp=searchResult.getInt(3)+temp;
				System.out.println("code number : "+searchResult.getInt(1)+" tablet number : "+searchResult.getString(2)+"  `Quantity : "+searchResult.getInt(3));
			
			}
//			System.out.println(temp);
			con.close();
			
		} catch (Exception e) {
			 
			e.printStackTrace();
		}

	}

}
